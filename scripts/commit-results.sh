# Copyright (c) 2019, Juniper Networks, Inc.
# All rights reserved.
#

source .env
set -e
if [ -z "$CI_COMMIT_REF_NAME" ]; then
  echo "no tag selected (CI_COMMIT_REF_NAME=$CI_COMMIT_REF_NAME)"
  exit 1
fi
git checkout master
git add -f logs/$CI_COMMIT_REF_NAME
git commit -m "test results $CI_COMMIT_REF_NAME"
git push
