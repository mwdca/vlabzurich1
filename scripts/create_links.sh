#!/bin/bash
# Copyright (c) 2019, Juniper Networks, Inc.
# All rights reserved.

sudo /bin/bash scripts/add_link.sh spine1 vmx1
sudo /bin/bash scripts/add_link.sh spine1 vmx2

sudo /bin/bash scripts/add_link.sh spine2 vmx1
sudo /bin/bash scripts/add_link.sh spine2 vmx2

sudo /bin/bash scripts/add_link.sh leaf1 spine1
sudo /bin/bash scripts/add_link.sh leaf1 spine2

sudo /bin/bash scripts/add_link.sh leaf2 spine1
sudo /bin/bash scripts/add_link.sh leaf2 spine2

sudo /bin/bash scripts/add_link.sh bms1  leaf1

sudo /bin/bash scripts/add_link.sh bms2  leaf2

sudo /bin/bash scripts/add_link.sh bms12 leaf1
sudo /bin/bash scripts/add_link.sh bms12 leaf2
