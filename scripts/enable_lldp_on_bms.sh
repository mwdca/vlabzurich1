#!/bin/bash
# Copyright (c) 2019, Juniper Networks, Inc.
# All rights reserved.

export COMPOSE_INTERACTIVE_NO_CLI=1 # avoid runner errors about 'the input device is not a TTY'

for server in bms1 bms2; do
  echo $server ...
  docker-compose exec $server apk add --no-cache open-lldp mtu tcpdump
  docker-compose exec $server lldpad -d 
  docker-compose exec $server lldptool set-lldp -i eth1 adminStatus=rxtx
  docker-compose exec $server lldptool -T -i eth1 -V sysName enableTx=yes
  docker-compose exec $server lldptool -T -i eth1 -V portDesc enableTx=yes
  docker-compose exec $server lldptool -T -i eth1 -V sysDesc enableTx=yes
  docker-compose exec $server lldptool -T -i eth1 -V sysCap enableTx=yes
  docker-compose exec $server lldptool -T -i eth1 -V mngAddr enableTx=yes
done

for server in bms1 bms2 bms12; do
  docker-compose exec $server apk add --no-cache open-lldp mtu tcpdump
  docker-compose exec $server lldpad -d 
  for interface in eth1 eth2; do
    docker-compose exec $server lldptool set-lldp -i $interface adminStatus=rxtx
    docker-compose exec $server lldptool -T -i $interface -V sysName enableTx=yes
    docker-compose exec $server lldptool -T -i $interface -V portDesc enableTx=yes
    docker-compose exec $server lldptool -T -i $interface -V sysDesc enableTx=yes
    docker-compose exec $server lldptool -T -i $interface -V sysCap enableTx=yes
    docker-compose exec $server lldptool -T -i $interface -V mngAddr enableTx=yes
    done
done
