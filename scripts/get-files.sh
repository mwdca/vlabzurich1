#!/bin/bash
# Copyright (c) 2018, Juniper Networks, Inc.
# All rights reserved.
#
# simple script to extract root passwords from vmx log file

set -e # exit if something goes wrong

source .env
DEST=tmplogs
mkdir -p $DEST

container=$(docker-compose ps -q contrail_command)
folder=$(docker exec $container ls -1 /var/tmp/contrail_cluster/|grep -v config)
docker cp $container:/var/tmp/contrail_cluster/$folder/instances.yml $DEST
sed -i "s/$CONTAINER_REGISTRY_PASSWORD/<set but hidden from output>/" $DEST/instances.yml 
docker cp $container:/var/log/ansible.log $DEST
head -30 $DEST/ansible.log > $DEST/ansible-summary.txt
echo "" >> $DEST/ansible-summary.txt
echo ". . ." >> $DEST/ansible-summary.txt
echo "" >> $DEST/ansible-summary.txt
grep -r 'PLAY RECAP\|changed=' $DEST/ansible.log >> $DEST/ansible-summary.txt
container=$(docker-compose ps -q contrail_command_init)
docker cp $container:/serv_dict.json $DEST
docker cp $container:/prov_dict.json $DEST
echo "ls -l $DEST"
ls -l $DEST

echo -n "checking for vrouter: in the created instances.yml file ..."
grep vrouter: $DEST/instances.yml || echo "NO!"

readme=$DEST/README.md
PROJECT="$(basename `pwd`)"
echo "# $PROJECT" > $readme
echo "" >> $readme
echo "CI_COMMIT_REF_NAME=$CI_COMMIT_REF_NAME" >> $readme
CONTRAIL_VERSION=$(grep CONTRAIL_VERSION $DEST/instances.yml|awk '{print $2}')
echo "CONTRAIL_VERSION=$CONTRAIL_VERSION"  >> $readme
echo "" >> $readme

start_time=$(head -1 $DEST/ansible.log |cut -d, -f1)
end_time=$(tail -1 $DEST/ansible.log |cut -d, -f1)
start_time_sec=$(date +%s -d "$start_time")
end_time_sec=$(date +%s -d "$end_time")
minutes=$(( ($end_time_sec - $start_time_sec) / 60 ))
echo "Total deployment time: $minutes minutes (from $start_time to $end_time)" >> $readme

echo "" >> $readme
echo "## Test platform:" >> $readme
uname -a >> $readme
echo "" >> $readme
lscpu|grep Model\ name >> $readme
echo "" >> $readme

echo "## Memory usage after install:" >> $readme
echo "\`\`\`" >> $readme
free -m >> $readme
echo "\`\`\`" >> $readme
echo "" >> $readme
echo "docker stats --no-stream" >>$readme
echo "\`\`\`" >> $readme
docker stats --no-stream >> $readme
echo "\`\`\`" >> $readme
