#!/bin/bash
# Copyright (c) 2018, Juniper Networks, Inc.
# All rights reserved.
#
# simple script to extract root passwords from vmx log file

set -e # exit if something goes wrong

source .env

echo "removing ssh host key from ~/.ssh/known_hosts for ..."
for device in $(docker-compose ps -q); do
  name=$(docker inspect --format='{{.Name}}' $device)
  name=$(basename $name)
  ip=$(docker logs $device 2>/dev/null |grep ' password '|cut -d\( -f2|cut -d\) -f1)
  if [ ! -z "$ip" ]; then
    ssh-keygen -f ~/.ssh/known_hosts -R $ip 2>/dev/null || true
    echo "$name ($ip)"
  fi
done
exit 0
