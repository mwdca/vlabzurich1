#!/bin/bash
# Copyright (c) 2019, Juniper Networks, Inc.
# All rights reserved.
#
set -e
source .env
DEST=tmplogs
mkdir -p $DEST
LOG=$DEST/test-results.txt
cp /dev/null $LOG

for script in tests/*.sh; do
  echo "===================================================================================" | tee -a $LOG
  echo "Running $script ..." | tee -a $LOG
  echo "===================================================================================" | tee -a $LOG
  $script | tee -a  $LOG
  if [ $? -ne 0 ]; then
    echo "ERROR: Last script aborted. Aborting tests"
    exit 1
  fi
done
